/*
 * File:   Lab4.h
 * Author: Maciej Przybylski
 *
 * Created on 15 maja 2016, 20:49
 */

#ifndef SWRM_LAB4_SETUP_DEMO_H
#define SWRM_LAB4_SETUP_DEMO_H

#include <opencv2/opencv.hpp>

#include <ros/ros.h>

#include <tf/transform_listener.h>

#include <sensor_msgs/PointCloud2.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>

namespace swrm_lab4_template{

class Lab4
{
public:
    explicit Lab4();

    virtual ~Lab4();

private:
    ros::NodeHandle nh_;

    tf::TransformListener tf_;

    ros::Subscriber sub_input_point_cloud_;
    ros::Publisher pub_output_point_cloud_;
    ros::Publisher pub_pose_array_;

    std::string user_frame_id_;

    pcl::visualization::PCLVisualizer viewer_;


    /**
     * Main function in which point cloud processing should be done.
     *
     * @param input
     */
    void processPointCloud(const sensor_msgs::PointCloud2ConstPtr& input);

    /**
     * Reduces pointcloud density using kD-Tree.
     * @param input_cloud
     * @param output_cloud
     * @param leaf_size [m]
     */
    void downsamplePointCloud(
                pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                double leaf_size);

    /**
     * Extracts the largest plane in a point cloud using RANSAC.
     *
     * The function seeks for the largest plane in \p input_cloud.
     * If one is found, its points are extracted from \p input_cloud and
     * placed in \p output_cloud.
     *
     * @param input_cloud It is modified if plan is found.
     * @param output_cloud Contains points belonging to a plane surface.
     * @param distance_threshold [m] Distance between point and plane model in meters.
     * @param max_iterations Number of RANSAC iterations.
     * @param normal_vector  A 3D vector for which a plane should perpendicular to.
     * @return Returns true if plane has been extracted.
     */
    bool extractPerpendicularPlane(
                pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                double distance_threshold,
                int max_iterations,
                Eigen::Vector3f normal_vector);

    /**
     * Simply removes points which are not in a range defined by \p limit_min and \p limit_max.
     *
     * @param input_cloud
     * @param output_cloud
     * @param field_name Name of tested value, e.g. "x"
     * @param limit_min [m]
     * @param limit_max [m]
     * @param invert If true points from a defined range are removed. (default false)
     */
    void getROI(
                pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                std::string field_name,
                double limit_min,
                double limit_max,
                bool invert = false);


    /**
     * Does segmentation of the \p input_cloud using region growing method.
     *
     * Each segment is stored in the \p segments as a separated pointcloud.
     *
     * @param input_cloud
     * @param segments
     * @param number_of_neighbours_for_normal_estimation e.g. 50
     * @param min_cluster_size number of points
     * @param max_cluster_size number of points
     * @param number_of_neighbours_for_region_growing
     * @param smoothness_threshold [rad] e.g. 5.0/180.0 * M_PI
     * @param curvature_threshold
     * @return true if any segment has been found
     */
    bool extractSegments(
                pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> &segments,
                int number_of_neighbours_for_normal_estimation,
                int min_cluster_size,
                int max_cluster_size,
                int number_of_neighbours_for_region_growing,
                double smoothness_threshold,
                double curvature_threshold);


    /**
     * Helpful function which removes points with NaN values.
     *
     * Should be used after point cloud transformation or plane extracting.
     *
     * @param input_cloud
     * @param output_cloud
     */
    void removeNaNFromPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud);

    /**
     * Converts 3D point cloud to a set of openCV 2D points by simple ignoring of a z value.
     *
     * @param segment
     * @return vector of openCV 2D points
     */
    std::vector<cv::Point2f> pointCloudToCvPoints2d(pcl::PointCloud<pcl::PointXYZ>::Ptr segment);



    void keyboardEvent(const pcl::visualization::KeyboardEvent& key);
};
}//namespace swrm_lab4_template
#endif /* SWRM_LAB4_SETUP_DEMO_H */
