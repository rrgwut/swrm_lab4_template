/*
 * File:   Lab4.cpp
 * Author: Maciej Przybylski
 *
 * Created on 15 maja 2016, 20:49
 */

#include "swrm_lab4_template/Lab4.h"

#include <tf_conversions/tf_eigen.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>

#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/PoseArray.h>

namespace swrm_lab4_template{

Lab4::Lab4() : nh_("~"), viewer_("Point Cloud Segmentation Viewer")
{
    sub_input_point_cloud_ = nh_.subscribe("/camera/depth_registered/points", 1,
            &Lab4::processPointCloud, this);

    pub_output_point_cloud_ = nh_.advertise<sensor_msgs::PointCloud2>("output_cloud", 1, false);
    pub_pose_array_ = nh_.advertise<geometry_msgs::PoseArray>("/fanuc_communication_node/pose_array", 1, false);

    nh_.param<std::string>("user_frame_id", user_frame_id_, "ar_marker_board");

    viewer_.registerKeyboardCallback(boost::bind(&Lab4::keyboardEvent,this,_1));
}

Lab4::~Lab4()
{

}


void Lab4::processPointCloud (const sensor_msgs::PointCloud2ConstPtr& input)
{
    sensor_msgs::PointCloud2 input_cloud_in_user_frame;

    //Transformuje punkty z układu kamery do układu user_frame_id_
    try{
        tf_.waitForTransform(user_frame_id_, input->header.frame_id, input->header.stamp, ros::Duration(2.0));

        pcl_ros::transformPointCloud(user_frame_id_,*input, input_cloud_in_user_frame, tf_);

    }catch(std::runtime_error const& e)
    {
        ROS_ERROR("%s",e.what());

        return;

    }

    // Konwersja z sensor_msgs::PointCloud2 do pcl::PointCloud<pcl::PointXYZ>
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (input_cloud_in_user_frame, *cloud);


    //Usuwa błędne pomiary z chmury punktów
    removeNaNFromPointCloud(cloud, cloud);


    //Ograniczenie chmury do punktów, których wartości z mieszczą się w przedziale <0,1.0> (metry) 
    getROI(cloud, cloud,"z", 0.0, 1.0);

    //Przerzedzanie chmury punktów - ma sens dla dużych obiektów
    //downsamplePointCloud(cloud, cloud,0.01);

    //Wyszukanie największej płaszczyzny prostopadłej do osi Z
    pcl::PointCloud<pcl::PointXYZ>::Ptr plane_1 (new pcl::PointCloud<pcl::PointXYZ>);
    extractPerpendicularPlane(cloud, plane_1, 
            0.04,                   //double distance_threshold [m]
            1000,                   //int max_iterations
            Eigen::Vector3f(0.0,0.0,1.0));

    //Czasami usunięcie błędnych punktów jest konieczne również po ekstrakcji największej płaszczyzny
    removeNaNFromPointCloud(plane_1, plane_1);
    

    //Największa płaszczyzna może zawierać górne powierzchnie kilku pudełek,
    //dlatego należy wykonać segmentację na odrębne obszary
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> segments;
    extractSegments(
                plane_1,
                segments,
                50,                 //int number_of_neighbours_for_normal_estimation,
                1,                  //int min_cluster_size,
                1000000,            //int max_cluster_size,
                20,                 //int number_of_neighbours_for_region_growing,
                10.0/180.0 *M_PI,   //double smoothness_threshold,
                5.0                 //double curvature_threshold
            );

    //Example of publishing of the object poses array
    geometry_msgs::PoseArray pose_array;
    pose_array.header.frame_id = user_frame_id_;
    pose_array.header.stamp = input->header.stamp;

    for(int i=0; i < segments.size(); ++i)
    {
        //Dodanie kolejnych segmentów do viewera
        pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZ>
                segment_color(segments[i]);
        viewer_.addPointCloud (segments[i],segment_color,"segment_" + boost::lexical_cast<std::string>(i));
        
        //Dopasowanie minimalnego prostokąta opisującego wykryte pudełko
        cv::RotatedRect rect = cv::minAreaRect(pointCloudToCvPoints2d(segments[i]));

        //Generation of an approach vector.
        geometry_msgs::Pose pose;
        pose.position.x = rect.center.x;
        pose.position.y = rect.center.y;
        pose.position.z = 0.1;              //wysokość pudełka, którą należy zmierzyć
        
        // wyznaczanie orientacji wektora podejścia
        pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(
                M_PI,                   //Wektor podejścia powinien być skierowany w dół, tj. przeciwnie niż oś Z
                0.0, 
                rect.angle*M_PI/180.0   //Wartość obliczana przez minAreaRect wyrażona jest w stopniach,
                                        //natomiast w ROS wymaga wartości w radianach
            );

        pose_array.poses.push_back(pose); //Dodanie pozycji do listy
    }
    
    pub_pose_array_.publish(pose_array);

    viewer_.spinOnce(10, true);
    viewer_.removeAllPointClouds();
    viewer_.removeAllShapes();
}

void Lab4::removeNaNFromPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud)
{
    std::vector<int> removed_indices2;
    pcl::removeNaNFromPointCloud(*input_cloud, *output_cloud, removed_indices2);
}

void Lab4::downsamplePointCloud(
                pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                double leaf_size)
{
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud (input_cloud);
    sor.setLeafSize (leaf_size, leaf_size, leaf_size);
    sor.filter (*output_cloud);
}


bool Lab4::extractPerpendicularPlane(
                pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                double distance_threshold,
                int max_iterations,
                Eigen::Vector3f normal_vector)
{
    if(input_cloud->points.size()==0)
        return false;

    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PERPENDICULAR_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (max_iterations);
    seg.setDistanceThreshold (distance_threshold);
    seg.setAxis(normal_vector);


    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (input_cloud);
    seg.segment (*inliers, *coefficients);

    if(inliers->indices.size()>0)
    {
        if(output_cloud.get()==0)
            output_cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>());

        // Extract the inliers

        // Create the filtering object
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud (input_cloud);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*output_cloud);

        extract.setNegative (true);
        extract.filterDirectly(input_cloud);

        return true;
    }

    return false;

}


void Lab4::getROI(
                pcl::PointCloud<pcl::PointXYZ>::ConstPtr input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud,
                std::string field_name,
                double limit_min,
                double limit_max,
                bool invert)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (input_cloud);
    pass.setFilterFieldName (field_name);
    pass.setFilterLimits (limit_min, limit_max);
    pass.setFilterLimitsNegative (invert);
    pass.filter (*output_cloud);
}

bool Lab4::extractSegments(
                pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud,
                std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> &segments,
                int number_of_neighbours_for_normal_estimation,
                int min_cluster_size,
                int max_cluster_size,
                int number_of_neighbours_for_region_growing,
                double smoothness_threshold,
                double curvature_threshold)
{
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod (tree);
    normal_estimator.setInputCloud (input_cloud);
    normal_estimator.setKSearch (number_of_neighbours_for_normal_estimation);
    normal_estimator.compute (*normals);

    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
    reg.setMinClusterSize (min_cluster_size);
    reg.setMaxClusterSize (max_cluster_size);
    reg.setSearchMethod (tree);
    reg.setNumberOfNeighbours (number_of_neighbours_for_region_growing);
    reg.setInputCloud (input_cloud);
    reg.setInputNormals (normals);
    reg.setSmoothnessThreshold (smoothness_threshold);
    reg.setCurvatureThreshold (curvature_threshold);

    std::vector <pcl::PointIndices> clusters;
    reg.extract (clusters);

    for(int i=0; i < clusters.size(); ++i)
    {
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud (input_cloud);
        pcl::PointIndices::Ptr indices(new pcl::PointIndices(clusters[i]));
        extract.setIndices (indices);
        extract.setNegative (false);

        pcl::PointCloud<pcl::PointXYZ>::Ptr segment = pcl::PointCloud<pcl::PointXYZ>::Ptr(
                new pcl::PointCloud<pcl::PointXYZ>());

        extract.filter (*segment);
        
        extract.setNegative (true);
        extract.filterDirectly(input_cloud);

        segments.push_back(segment);
    }

    return !segments.empty();
}

std::vector<cv::Point2f> Lab4::pointCloudToCvPoints2d(pcl::PointCloud<pcl::PointXYZ>::Ptr segment)
{
    std::vector<cv::Point2f> points_2f;

    for(size_t i=0; i<segment->size(); ++i)
    {
        cv::Point2f point(segment->at(i).x, segment->at(i).y);
        points_2f.push_back(point);
    }

    return points_2f;
}


void Lab4::keyboardEvent(const pcl::visualization::KeyboardEvent& key)
{
    if(key.getKeyCode()==char(27))
        ros::shutdown();
}

}//namespace swrm_lab4_template
