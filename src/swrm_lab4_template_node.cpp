/*
 * File:   swrm_lab4_demo_node.cpp
 * Author: Maciej Przybylski
 *
 * Created on 15 maja 2016, 20:53
 */

#include <cstdlib>

#include <ros/ros.h>

#include <swrm_ros/UniqueNodeName.h>

#include "swrm_lab4_template/Lab4.h"

/*
 *
 */
int main(int argc, char** argv)
{
    std::string node_name = swrm_ros::uniqueNodeName("swrm_lab4_template_node");

    ros::init(argc, argv, node_name.c_str());

    swrm_lab4_template::Lab4 lab4;

    ros::Rate loop_rate(20);

    while(ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
